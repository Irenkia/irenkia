const { Schema } = require('mongoose');
const connections = require('../../config/connection');

const StringSchema = new Schema(
    {
        text: {
            type: String,
            trim: true,
            required: true,
        },
    },
    {
        collection: 'stringmodel',
        versionKey: false,
    },
);

module.exports = connections.model('StringModel', StringSchema);
