module.exports = {
    extends: [
        'eslint:recommended',
        'plugin:node/recommended',
        'airbnb-base',
        'plugin:security/recommended',
        'plugin:sonarjs/recommended',
    ],
    env: {
        es6: true,
        node: true,
    },
    plugins: [
        'node',
        'sonarjs',
        'security',
    ],
    parserOptions: {
        ecmaVersion: 2020,
    },
    rules: {
        indent: [
            'error',
            4,
        ],
        'no-console': 0,
        'no-fallthrough': 0,
    },
};
